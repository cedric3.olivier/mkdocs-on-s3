
# Bar

Java code snippet:

```java
public class Bar {
  public int pour(Drink drink, Glass glass) {
    return drink.pour(glass);
  }
}
```
